# -*- coding: utf-8 -*-
'''
@file       FSM_Elevator.py
@brief      Simulates a two storey elevator
@details    Implements a finite state machine to simulate the behavior of a two storey elevator.
@author     Ahkar Kyaw
@date       1/23/2021
'''

import random

def motor(cmd):
    """
    @brief      Commands the motor to move to floor 1, floor 2 or stop.
    @param cmd  The Command to give the motor
    """
    if cmd=='0':
        print('Mot stop')
    elif cmd=='1':
        print('Mot upward')
    elif cmd=='2':
        print('Mot downward')

def first():
    return random.choice([True,False]) # Randomly returns T or F

def second():
    return random.choice([True,False]) # Randomly returns T or F

def button(one,two):
    """
    @brief      Request button input from user
    @param one  State of button_1
    @param two  State of button_2
    """
    button = input('Please enter the floor you wish to go! \n \t floor: ')
    if button=='1':
        one = 1
    elif button=='2':
        two = 1
    else:
        print('Invalid floor! \n')
    return one,two;


# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the scropt is imported as a module
if __name__ == '__main__':
    # Program initialization:
    state = 0 # Initial state is the INIT state
    
    while True:
        try:
            # Main program code:
            if state==0:
                # Run state(0) INIT code
                print('S0 [INIT]')
                motor('2')
                button_2 = 0
                state = 1
            elif state==1:
                # Run state(1) MOVING DOWN code
                print('S1 [MOVING DOWN]')
                # If the elevator reach floor 1,
                if first():
                    motor('0')
                    button_1 = 0
                    state = 2
            elif state==2:
                # Run state(2) STOPPED ON FLOOR 1 code
                print('S2 [ON FLOOR 1]')
                button_1,button_2 = button(button_1,button_2)
                if button_1==1:
                    button_1 = 0
                elif button_2==1:
                    motor('1')
                    state = 3
            elif state==3:
                # Run state(3) MOVING DOWN code
                print('S3 [MOVING UP]')
                # If the elevator reach floor 2,
                if second():
                    motor('0')
                    button_2 = 0
                    state = 4
            elif state==4:
                # Run state(4) STOPPED ON FLOOR 2 code
                print('S4 [ON FLOOR 2]')
                button_1,button_2 = button(button_1,button_2)
                if button_2==1:
                    button_2 = 0
                elif button_1==1:
                    motor('2')
                    state = 1
                    
        except KeyboardInterrupt:
            # This except block catches "Ctrl+C" from the keyboard to end the
            # while(True) loop when desired
            print('\nSee you next time!')
            break
